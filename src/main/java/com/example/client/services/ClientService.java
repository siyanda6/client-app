package com.example.client.services;

import com.example.client.constants.ClientConstants;
import com.example.client.entities.Client;
import com.example.client.mappings.ClientMapper;
import com.example.client.utils.CsvUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientService {

    @Autowired
    CsvUtil csvUtil;

    public Client save(Client client) {
        return csvUtil.save(client);
    }

    public Client update(Client client) {
        return csvUtil.update(client);
    }

    public List<Client> getClientsByFirstName(String firstName) {
        return csvUtil.searchByField(ClientConstants.FIRST_NAME, firstName);
    }

    public List<Client> getClientsByLastName(String lastName) {
        return csvUtil.searchByField(ClientConstants.LAST_NAME, lastName);
    }

    public Client getClientByMobileNumber(String mobileNumber) {
        return csvUtil.searchByField(ClientConstants.MOBILE_NUMBER, mobileNumber).stream().findFirst().orElse(null);
    }

    public Client getClientByIdNumber(String idNumber) {
        return csvUtil.searchByField(ClientConstants.ID_NUMBER, idNumber).stream().findFirst().orElse(null);
    }
}
