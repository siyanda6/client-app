package com.example.client.utils;

import com.example.client.constants.ClientConstants;
import com.example.client.entities.Client;
import com.opencsv.CSVWriter;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CsvUtil {

    public List<Client> searchByField(String key, String value) {

        List<Client> clients = getAllClients();

        switch (key) {
            case ClientConstants.FIRST_NAME:
                return clients.stream().filter(r -> r.getFirstName().equals(value)).collect(Collectors.toList());
            case ClientConstants.LAST_NAME:
                return clients.stream().filter(r -> r.getLastName().equals(value)).collect(Collectors.toList());
            case ClientConstants.MOBILE_NUMBER:
                return clients.stream().filter(r -> r.getMobileNumber().equals(value)).collect(Collectors.toList());
            case ClientConstants.ID_NUMBER:
                return clients.stream().filter(r -> r.getIdNumber().equals(value)).collect(Collectors.toList());
        }

        return null;
    }

    public Client save(Client client) {

        String[] CSV_HEADER = { ClientConstants.FIRST_NAME,
                                ClientConstants.ID_NUMBER,
                                ClientConstants.LAST_NAME,
                                ClientConstants.MOBILE_NUMBER,
                                ClientConstants.PHYSICAL_ADDRESS };

        try {
            FileWriter writer = new FileWriter(ClientConstants.CSV_PATH, true);

            List<Client> clients = new ArrayList<>();
            clients.add(client);

            ColumnPositionMappingStrategy mappingStrategy=
                    new ColumnPositionMappingStrategy();
            mappingStrategy.setType(Client.class);

            mappingStrategy.setColumnMapping(CSV_HEADER);

            // Creating StatefulBeanToCsv object
            StatefulBeanToCsvBuilder<Client> builder=
                    new StatefulBeanToCsvBuilder(writer);
            StatefulBeanToCsv beanWriter =
                    builder.withMappingStrategy(mappingStrategy).withQuotechar(CSVWriter.NO_QUOTE_CHARACTER).build();

            // Write list to StatefulBeanToCsv object
            beanWriter.write(clients);

            // closing the writer object
            writer.close();

        } catch (Exception e) {
            System.out.println("Writing CSV error!");
            e.printStackTrace();
        }

        return client;
    }

    public Client update(Client client) {

        String[] CSV_HEADER = { ClientConstants.FIRST_NAME,
                ClientConstants.LAST_NAME,
                ClientConstants.MOBILE_NUMBER,
                ClientConstants.ID_NUMBER,
                ClientConstants.PHYSICAL_ADDRESS };

        List<Client> clients = getAllClients();

        List<Client> newClients = clients.stream().map(r -> r.getIdNumber().equals(client.getIdNumber()) ||
                            r.getMobileNumber().equals(client.getMobileNumber()) ? client : r)
                .collect(Collectors.toList());

        try {
            FileWriter writer = new FileWriter(ClientConstants.CSV_PATH);

            ColumnPositionMappingStrategy mappingStrategy=
                    new ColumnPositionMappingStrategy();
            mappingStrategy.setType(Client.class);

            mappingStrategy.setColumnMapping(CSV_HEADER);

            // Creating StatefulBeanToCsv object
            StatefulBeanToCsvBuilder<Client> builder=
                    new StatefulBeanToCsvBuilder(writer);
            StatefulBeanToCsv beanWriter =
                    builder.withOrderedResults(false).withQuotechar(CSVWriter.NO_QUOTE_CHARACTER).build();

            // Write list to StatefulBeanToCsv object
            beanWriter.write(newClients);

            // closing the writer object
            writer.close();

        } catch (Exception e) {
            System.out.println("Writing CSV error!");
            e.printStackTrace();
        }

        return client;
    }

    public List<Client> getAllClients() {

        FileReader fileReader;
        CSVParser csvParser;
        List<Client> clients = new ArrayList<>();

        try {
            fileReader = new FileReader(ClientConstants.CSV_PATH);
            csvParser = new CSVParser(fileReader,
                    CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());

            Iterable<CSVRecord> csvRecords = csvParser.getRecords();

            for (CSVRecord csvRecord : csvRecords) {
                Client client = new Client(csvRecord.get(ClientConstants.FIRST_NAME),
                        csvRecord.get(ClientConstants.LAST_NAME),
                        csvRecord.get(ClientConstants.MOBILE_NUMBER),
                        csvRecord.get(ClientConstants.ID_NUMBER),
                        csvRecord.get(ClientConstants.PHYSICAL_ADDRESS));

                clients.add(client);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return clients;
    }
}
