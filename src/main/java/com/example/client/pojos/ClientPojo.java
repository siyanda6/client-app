package com.example.client.pojos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientPojo {

    private String firstName;
    private String lastName;
    private String mobileNumber;
    private String idNumber;
    private String physicalAddress;
}
