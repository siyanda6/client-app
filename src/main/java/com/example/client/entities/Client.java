package com.example.client.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Client {

    public String firstName;
    public String lastName;
    public String mobileNumber;
    public String idNumber;
    public String physicalAddress;

}
