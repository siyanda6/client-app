package com.example.client.mappings;

import com.example.client.entities.Client;
import com.example.client.pojos.ClientPojo;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;


import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR, uses = {})
public interface ClientMapper {

     ClientPojo fromEntityToPojo(Client entity);
     List<ClientPojo> fromEntitiesToPojos(List<Client> entities);

     Client fromPojoToEntity(ClientPojo pojo);
     List<Client> fromPojosToEntities(List<ClientPojo> pojos);
}
