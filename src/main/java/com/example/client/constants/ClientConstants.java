package com.example.client.constants;

public class ClientConstants {

    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String MOBILE_NUMBER = "mobileNumber";
    public static final String ID_NUMBER = "idNumber";
    public static final String PHYSICAL_ADDRESS = "physicalAddress";

    public static final String CSV_PATH = "clients.csv";
}
