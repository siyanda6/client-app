package com.example.client.controllers;

import com.example.client.entities.Client;
import com.example.client.services.ClientService;
import com.example.client.validators.Validator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping(value = "clients", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
public class ClientController {

    @Autowired
    ClientService clientService;

    @Autowired
    Validator validator;

    @PostMapping
    public ResponseEntity<Client> saveClient(@RequestBody Client client) {

        try {

            if(validator.validateFields(client) != null) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            }

            if (validator.existingIdNumber(client.getIdNumber())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            }

            if (validator.existingMobileNumber(client.getMobileNumber())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            }

            client = clientService.save(client);

            return ResponseEntity.ok(client);

        } catch (Exception ex) {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping
    public ResponseEntity<Client> updateClient(@RequestBody Client client) {

        try {

            if(validator.validateFields(client) != null) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            }

            if (validator.existingClientWithMobileNumberOrIdNumber(client.getMobileNumber(),
                                                                    client.getIdNumber()) != null) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
            }

            client = clientService.update(client);

            return ResponseEntity.ok(client);

        } catch (Exception ex) {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/search/{firstName}/by-first-name")
    public ResponseEntity<List<Client>> searchByFirstName(@PathVariable String firstName) {

        try {
            List<Client> clients = clientService.getClientsByFirstName(firstName);

            return ResponseEntity.ok(clients);

        } catch (Exception ex) {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/search/{lastName}/by-last-name")
    public ResponseEntity<List<Client>> searchByLastName(@PathVariable String lastName) {

        try {
            List<Client> clients = clientService.getClientsByLastName(lastName);

            return ResponseEntity.ok(clients);

        } catch (Exception ex) {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/search/{mobileNumber}/by-mobile-number")
    public ResponseEntity<Client> searchByMobileNumber(@PathVariable String mobileNumber) {

        try {
            Client client = clientService.getClientByMobileNumber(mobileNumber);

            return ResponseEntity.ok(client);

        } catch (Exception ex) {

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
