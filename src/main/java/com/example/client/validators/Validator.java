package com.example.client.validators;

import com.example.client.entities.Client;
import com.example.client.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class Validator {

    @Autowired
    ClientService clientService;

    public boolean existingIdNumber(String idNumber) {

        if(clientService.getClientByIdNumber(idNumber) != null) {
            return true;
        }

        return false;
    }

    public boolean existingMobileNumber(String mobileNumber) {

        if(clientService.getClientByMobileNumber(mobileNumber) != null) {
            return true;
        }

        return false;
    }

    public String existingClientWithMobileNumberOrIdNumber(String mobileNumber, String idNumber) {

        Client client = clientService.getClientByMobileNumber(mobileNumber);

        if(client != null) {
            if(idNumber.equals(client.getIdNumber())){
                return null;
            }else{
                return "Updating a record with an ID number or mobile number already in the system is not allowed.";
            }
        }else if(clientService.getClientByIdNumber(idNumber) == null) {
            return "The record you are trying to update does not exist";
        }

        return null;
    }

    public String validateFields(Client client) {

        if(!StringUtils.hasText(client.getFirstName())){
            return "First Name is required";
        }

        if(!StringUtils.hasText(client.getLastName())){
            return "Last Name is required";
        }

        if(!StringUtils.hasText(client.getIdNumber())){
            return "Id Number is required";
        }

        if(client.getIdNumber().length() != 13){
            return "ID Number is not a valid South African ID, it must contain 13 digits";
        }

        return null;
    }
}
