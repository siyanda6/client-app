package com.example.client.services;

import com.example.client.entities.Client;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ClientServiceTest {

    @Autowired
    ClientService clientService;

    @Test
    public void saveClient() {
        Client client = clientService.save(mockClient());
    }

    @Test
    public void updateClient() {
        Client client = clientService.update(mockUpdateClient());
    }

    @Test
    public void getClientsByFirstName() {
        List<Client> clients = clientService.getClientsByFirstName("Siyanda");
        assert(clients.size() > 0);
    }

    @Test
    public void getClientsByLastName() {
        List<Client> clients = clientService.getClientsByLastName("Wanda");
        assert(clients.size() > 0);
    }

    @Test
    public void getClientsByPhoneNumber() {
        Client client = clientService.getClientByMobileNumber("0720900034");
        assert(client != null);
    }

    private Client mockClient() {

        Client client = new Client("Siyanda",
                                    "Wanda",
                                    "0764511123",
                                    "8809211915082",
                                    "Test Address");

        return client;
    }

    private Client mockUpdateClient() {

        Client client = clientService.getClientByMobileNumber("0764511123");
        client.setFirstName("Bright");
        return client;
    }
}
