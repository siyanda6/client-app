package com.example.client.validators;

import com.example.client.entities.Client;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ValidatorTest {

    @Autowired
    Validator validator;

    @Test
    public void existingIdNumberTest() {
        boolean result = validator.existingIdNumber("8809211915082");
        assert(result);
    }

    @Test
    public void existingMobileNumberTest() {
        boolean result = validator.existingMobileNumber("0764511123");
        assert(result);
    }

    @Test
    public void existingMobileNumberOrIdNumberTest() {
        String result = validator.existingClientWithMobileNumberOrIdNumber("0764511123", "8809211915083");
        assert(result != null);
    }

    @Test
    public void validateFieldsTest() {
        String result = validator.validateFields(mockClient());
        assert(result == null);
    }

    @Test
    public void validateFieldsWithoutFirstTest() {
        String result = validator.validateFields(mockWithoutFirstClient());
        assert(result == null);
    }

    @Test
    public void validateFieldsWithoutLastTest() {
        String result = validator.validateFields(mockWithoutLaststClient());
        assert(result == null);
    }

    @Test
    public void validateFieldsWithoutIdTest() {
        String result = validator.validateFields(mockWithoutIDtClient());
        assert(result == null);
    }

    @Test
    public void validateFieldsWithInvalidIdTest() {
        String result = validator.validateFields(mockWithInvalidIDClient());
        assert(result == null);
    }

    private Client mockClient() {

        Client client = new Client("Siyanda",
                "Wanda",
                "0764511123",
                "8809211915082",
                "Test Address");

        return client;
    }

    private Client mockWithoutFirstClient() {

        Client client = new Client(null,
                "Wanda",
                "0764511123",
                "8809211915082",
                "Test Address");

        return client;
    }

    private Client mockWithoutLaststClient() {

        Client client = new Client("Siyanda",
                null,
                "0764511123",
                "8809211915082",
                "Test Address");

        return client;
    }

    private Client mockWithoutIDtClient() {

        Client client = new Client("Siyanda",
                "Wanda",
                "0764511123",
                null,
                "Test Address");

        return client;
    }

    private Client mockWithInvalidIDClient() {

        Client client = new Client("Siyanda",
                "Wanda",
                "0764511123",
                "88092119150827777",
                "Test Address");

        return client;
    }
}
